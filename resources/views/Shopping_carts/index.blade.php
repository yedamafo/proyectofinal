@extends('layouts.app')

@section('content')
<div class="big-padding tex-center blue-grey white-text">
    <h1>Tu carrito de compras</h1>
</div>
<div class="container">
    <table class="table table-bordered">
        <thead>
            <tr>
                <td>producto</td>
                <td>precio</td>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
            <tr>
                <td>{{$product->title}}</td>
                <td>{{$product->pricing}}</td>
            </tr>
            @endforeach
            <tr>
                <td>total</td>
                <td>{{$total}}</td>
            </tr>
        </tbody>
    </table>
    

    <div class="text-right">
        <table align="right">
        <tr>
    <td><a href="{{url('/products')}}">Regrese al listado de productos</a><br></td>
        <td>@include("shopping_carts.form")</td>
<tr>
</table>
       
    </div>
</div>
   
@endsection



