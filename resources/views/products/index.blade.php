@extends('layouts.app')


@section("content")

<div class="big-padding text-center blue-grey white-text">
	<marquee  width="50%" height="60" align="bottom" direction="left">
	<h1><b>Productos</b></h1>
</marquee>
	
</div>
<div class="container">
	<table  width="105%" border="3" class="table-bordered">
		<thead>
			<td align="center" width="3%"><b>ID</b></td>
			<td align="center" width="9%"><b>Titulo</b></td>
			<td align="center"><b>Descripcion</b></td>
			<td align="center"><b>Precio</b></td>
			<td align="center" width="16%"><b>Acciones</b></td>
		</thead>
		<tbody>
			@foreach ($products as $product)
			<tr>
				<td align="center">{{$product->id}}</td>
				<td align="center">{{$product->title}}</td>
				<td>{{$product->description}}</td>
				<td>{{$product->pricing}}</td>
				<td>
					<center>
					<table width="9%"  border="0">
  <tr>
  <td><div class="absolute actions">
             <a href="{{url('/products/'.$product->id.'/edit')}}" class="btn btn-primary">Editar</a></td>
  
         

         <td> @include('products.delete',['product'=>$product])</font></td>
         <td> <a href="{{url('/products/'.$product->id.'')}}" class="btn btn-success">VER</a></td>

         

  </tr>
 </table>
 </center>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

<div  class="floating text-right">
		<a href="{{'products/create'}}" class=" btn btn-primary btn-fab">
			<i class="material-icons">add</i>
		</a>

</div>
@endsection