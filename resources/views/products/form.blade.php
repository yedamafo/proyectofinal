
	{!! Form::open(['url' =>$url,'method' =>$method])!!}
	<div class="form-grup">
{{Form::text('title',$product->title,['class'=>'form-control','placeholder'=>'titulo...'])}}
	</div>
		
		<div class="form-grup">
			{{Form::number('pricing',$product->pricing,['class'=>'form-control','placeholder'=>'Precio de tu producto en centavos de dolar'])}}
		</div>
	<div class="form-grup">
		{{Form::textarea('description',$product->description,['class'=>'form-control','placeholder'=>'Describe tu producto...'])}}
	</div>
	<div class="form-grup text-right">
		<a href="{{url('/products')}}">Regrese al listado de productos</a>
		<input type="submit" value="Enviar" class="btn btn-success">
	</div>
	{!!Form::close()!!}