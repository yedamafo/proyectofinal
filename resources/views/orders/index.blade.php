@extends("layouts.app")


@section("content")

<div class="container">
<div class="panel panel-default">
	<div class="panel-heading">
		<center>
		<h2>Dashboard</h2>
		</center>
	</div>
	<div class="panel-body">
		<h3>Estadisticas</h3>

<center>
		<div class="row top-space">
	<div class="col-xs-4 col-md-3 col-lg-2 sale-data">
	<span>{{$totalMonth}}USD</span>
	<br>Ingresos del mes
		</div>

	<div class="col-xs-4 col-md-3 col-lg-2 sale-data">
	<span>{{$totalMonthCount}}</span>
	<br>Numero de ventas
		</div>


		</div>
	</center>
<center>
		<h3>Ventas</h3>
		</center>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td>ID. venta</td>
					<td>Comprador</td>
					<td>Direccion</td>
					
					<td>Status</td>
					<td>Fecha de venta</td>
					<td>Acciones</td>
             </tr>
			</thead>
			<tbody>
				@foreach ($orders as $order)
				<tr>
					<td>{{$order->id}}</td>
					<td>{{$order->recipient_name}}</td>
					<td>{{$order->address()}}</td>
					
					<td>{{$order->status}}</td>
					<td>{{$order->created_at}}</td>
					<td>Acciones</td>


				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
    </div>
	</div>

	@endsection