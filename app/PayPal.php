<?php


namespace App;

class PayPal{

	private $_apiContext;
	private $shopping_cart;
	private $_ClientId ='AWONcewQb9W0MUMHQSGHogx4n-8NNZcB5PA_kJc2oHqklathH27_pLZpCHcLBisevxUq7lH55xYMdJj9';
	private $_ClientSecret ='EMJ3imzml75fzmAvRb5wWYSUmd61EyMTSgXFDZ0UOFg9NszcIC2GIfn1xwLW0HugQlK0GnEfP46ujrdJ';


	public function __construct($shopping_cart){

		$this->_apiContext = \PaypalPayment::ApiContext($this->_ClientId, $this->_ClientSecret);
         
         $config= config("paypal_payment");
         $flatConfig = array_dot($config);

         $this->_apiContext->setConfig($flatConfig);

		$this->shopping_cart=$shopping_cart;
	}
	
	public function generate(){

	$payment = \PaypalPayment::payment()->setIntent("sale")->setPayer($this->payer())->setTransactions([$this->transaction()])->setRedirectUrls($this->redirectURLs());

	try{
     $payment->create($this->_apiContext);

	}catch(\Exception $ex){
		dd($ex);
		exit(1);
	}

	return $payment;
	}

	public function payer(){

       return \PaypalPayment::payer()->setPaymentMethod("paypal");
	}

	

	public function redirectURLs(){

         $baseURL = url('/');
         return \PaypalPayment::redirectUrls()->setReturnUrl("$baseURL/payments/store")->setCancelUrl("$baseURL/carrito");
	}

	public function transaction(){

	return \PaypalPayment::transaction()->setAmount($this->amount())->setItemList($this->items())->setDescription("Tu compra en Hunter X Hunter")->setInvoiceNumber(uniqid());

	} 
 
	public function items(){
    
     $items=[];
     $products=$this->shopping_cart->products()->get();
		
		foreach ($products as $product){
			array_push($items, $product->paypalItem());

		}

		return \PaypalPayment::itemList()->setItems($items);
	}

	public function amount(){

	return \PaypalPayment::amount()->setCurrency("USD")->setTotal($this->shopping_cart->totalUSD());
	}
public function execute($paymentId,$PayerID){
	$payment = \PaypalPayment::getById($paymentId,$this->_apiContext);
	$execution = \PaypalPayment::PaymentExecution()->setPayerID($PayerID);
	
	return $payment->execute($execution,$this->_apiContext);
}
	
      
}