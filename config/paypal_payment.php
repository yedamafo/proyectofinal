 <?php

return array(
	# Account credentials from developer portal
	'Account' => array(
		'ClientId' => env('AWONcewQb9W0MUMHQSGHogx4n-8NNZcB5PA_kJc2oHqklathH27_pLZpCHcLBisevxUq7lH55xYMdJj9'),
		'ClientSecret' => env('EMJ3imzml75fzmAvRb5wWYSUmd61EyMTSgXFDZ0UOFg9NszcIC2GIfn1xwLW0HugQlK0GnEfP46ujrdJ'),
	),

	# Connection Information
	'Http' => array(
		// 'ConnectionTimeOut' => 30,
		'Retry' => 1,
		//'Proxy' => 'http://[username:password]@hostname[:port][/path]',
	),

	# Service Configuration
	'Service' => array(
		# For integrating with the live endpoint,
		# change the URL to https://api.paypal.com!
		'EndPoint' => 'https://api.sandbox.paypal.com',
	),


	# Logging Information
	'Log' => array(
		//'LogEnabled' => true,

		# When using a relative path, the log file is created
		# relative to the .php file that is the entry point
		# for this request. You can also provide an absolute
		# path here
		'FileName' => '../PayPal.log',

		# Logging level can be one of FINE, INFO, WARN or ERROR
		# Logging is most verbose in the 'FINE' level and
		# decreases as you proceed towards ERROR
		//'LogLevel' => 'FINE',
	),

	# Define your application mode here
	'mode' => 'sandbox'
);
